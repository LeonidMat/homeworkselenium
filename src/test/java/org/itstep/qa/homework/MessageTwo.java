package org.itstep.qa.homework;/*Написать второй тест, который проверяе что блок адреса
корректно обрабатывает неполное наименование адреса:
в поле поиска вводится адре с виде "Москва Арбат 11 2"
и проверяется что форма корректно дополнила недостающими
символами адрес, например "д. 11", в том числе и для строки поиска*/


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class MessageTwo {
    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver",
                "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

//    @AfterClass
//    public void quitBrowser() {
//
//    }

    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void findAddressTest() throws InterruptedException {
        driver.get("http://hflabs.github.io/suggestions-demo/");

        WebElement element =
                driver.findElement(By.id("address"));
        element.click();
        element.sendKeys("Москва Арбат 11 2");
        Thread.sleep(1000);
        clickOnSuggestText("г Москва, ул Новый Арбат, д 11 стр 2", driver);
        element.click();
    }

    private void clickOnSuggestText(String str, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='address']/parent::div//div[@class='suggestions-suggestions']")));
        List<WebElement> elementList = driver
                .findElements(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[2]/div[1]/div/div"));
        WebElement[] elements = elementList.toArray(new WebElement[elementList.size()]);
        for (WebElement web : elements) {
            if (web.getText().equals(str)) {
                web.click();
                break;
            }
        }
    }
}