package org.itstep.qa.homework;/*На странице http://hflabs.github.io/suggestions-demo/
написать автотест на проверку заполнения блока адреса:
вводите в строку поиска некоторый адрес и проверяете,
что ВСЕ поля блока адреса заполняются*/


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class Message {
    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver",
                "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

//    @AfterClass
//    public void quitBrowser() {
//
//    }

    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void findAddressTest() throws InterruptedException {
        driver.get("http://hflabs.github.io/suggestions-demo/");

        WebElement element =
                driver.findElement(By.id("address"));
        element.click();
        element.sendKeys("г Москва, пр-кт Будённого, д 31 стр 9" );
        Thread.sleep(1000);
        element.sendKeys(Keys.ENTER);

        WebElement element2 = driver.findElement(By.id("address-city"));
        Assert.assertEquals(element2.getText(),"г Москва");


    }

//        clickOnSuggestText("г Москва, пр-кт Будённого, д 31 стр 9" , driver);
//    private void clickOnSuggestText(String str, WebDriver driver) {
//        WebDriverWait wait = new WebDriverWait(driver, 5);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='address']/parent::div//div[@class='suggestions-suggestions']")));
//        List<WebElement> elementList = driver
//                .findElements(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[2]/div[1]/div/div"));
//        WebElement[] elements = elementList.toArray(new WebElement[elementList.size()]);
//        for (WebElement web : elements) {
//            if (web.getText().equals(str)) {
//                web.click();
//                break;
//            }
//        }
//
//    }
}
