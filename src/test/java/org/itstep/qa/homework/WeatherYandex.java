package org.itstep.qa.homework;/*Создать тестовый проект в Intellij IDEA с использованием maven.
Добавить зависимости для Selenium и TestNG. Создать тест для
поиска строки “Погода в Гомеле” в поисковых системах  Google и Yandex.*/

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WeatherYandex {
    private WebDriver driver;
    @BeforeClass
    public void createBrowser(){
        System.setProperty("webdriver.chrome.driver",
                "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @AfterClass
    public void qoitBrowser(){

    }
    @BeforeMethod
    public void clear(){
        driver.manage().deleteAllCookies();
    }
    @Test
    public void findInGoogleTest(){
        driver.get("https://yandex.by");
        WebElement webElement=
                driver.findElement(By.xpath("//*[@id=\"text\"]"));
        webElement.sendKeys("Погода в Гомеле"+ Keys.ENTER);
        webElement=driver.findElement(By.xpath("/html/body/div[3]/div[1]/div[2]/div[1]/div[1]/ul/li[1]/div/div[1]/h2/a/div[2]"));
        Assert.assertEquals(webElement.getText(), "Погода в Гомеле");

    }
}
